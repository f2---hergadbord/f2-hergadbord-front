import { makeStyles } from "@material-ui/styles";

const Style = makeStyles({
  main: {},
  close: {
    padding: "10px"
  },
  succes: {
    backgroundColor: "green !important",
    background: "green !important"
  },
  error: {
    backgroundColor: "red !important",
    background: "red !important"
  },
  info: {
    backgroundColor: "#7c9bcc !important",
    background: "#7c9bcc !important"
  },
  warning: {
    backgroundColor: "yellow"
  }
});

export default Style;
