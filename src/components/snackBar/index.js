import React, { useContext } from "react";
import { useDispatch, connect } from "react-redux";

import Snackbar from "@material-ui/core/Snackbar";
import SnackbarContent from "@material-ui/core/SnackbarContent";
import PropTypes from "prop-types";
import clsx from "clsx";

import Style from "./style";
import { I18nContext } from "../../language/translate";
import actions from "../../redux/snackBar/action";

const { resetSnackBar } = actions;

function SnackBar(props) {
  const classes = Style();

  const { langCode } = useContext(I18nContext);
  const dispatch = useDispatch();
  const { open, type, messageFR, messageEN } = props;

  function handleClose(event) {
    if (event) event.preventDefault();
    dispatch(resetSnackBar());
  }

  return (
    <Snackbar
      anchorOrigin={{
        vertical: "top",
        horizontal: "right"
      }}
      open={open}
      autoHideDuration={6000}
      onClose={e => handleClose(e)}
      // TransitionComponent={<Slide direction="up" />}
    >
      <SnackbarContent
        className={clsx(classes[type])}
        onClose={e => handleClose(e)}
        message={langCode === "fr" ? messageFR : messageEN}
        aria-describedby="client-snackbar"
      />
    </Snackbar>
  );
}

SnackBar.propTypes = {
  open: PropTypes.bool,
  type: PropTypes.string,
  messageFR: PropTypes.string,
  messageEN: PropTypes.string
};
SnackBar.defaultProps = { open: false, type: "", messageFR: "", messageEN: "" };

const mapStateToProps = state => ({
  open: state.SnackBar.open,
  type: state.SnackBar.type,
  messageFR: state.SnackBar.messageFR,
  messageEN: state.SnackBar.messageEN
});

export default connect(mapStateToProps)(SnackBar);
