export const server = `${process.env.REACT_APP_SERVEUR_HOST}`;

export const client = `${process.env.REACT_APP_CLIENT_HOST}`;

export const usersLogin = `${process.env.REACT_APP_SERVEUR_HOST}/users/login`;
export const users = `${process.env.REACT_APP_SERVEUR_HOST}/users`;
export const rooms = `${process.env.REACT_APP_SERVEUR_HOST}/rooms`;
export const roomsUser = `${process.env.REACT_APP_SERVEUR_HOST}/rooms/user`;
