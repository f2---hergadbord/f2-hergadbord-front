import { take, call, race, put, select } from "redux-saga/effects";
// import { push } from "react-router-redux";
import actions from "./action";
import onGetJson from "../onGetJson";
import actionsSnackBar from "../snackBar/action";
import { users } from "../../api";

// import history from "../../history";

const getToken = state => state.Auth.token;

export function* modifyUser(info) {
  const token = yield select(getToken);
  const server = `${users}/${info.info.id}`;
  const password = info.info.password !== null ? info.info.password : undefined;
  const authorization = `Bearer ${token}`;
  const body = {
    method: "PATCH",

    headers: {
      "Content-type": "application/json",
      Accept: "application/json",
      authorization
    },
    body: JSON.stringify({
      idAvatar: info.info.idAvatar,
      email: info.info.email,
      password,
      pseudo: info.info.pseudo
    })
  };

  try {
    const result = yield call(onGetJson, body, server);
    switch (result.status) {
      case 200:
        yield put({
          type: actions.UPDATE_USER,
          payload: {
            email: result.body.email,
            idAvatar: result.body.idAvatar,
            pseudo: result.body.pseudo,
            id: result.body.id
          }
        });
        yield put({ type: actionsSnackBar.SET_OK_MODIFY_USER });
        break;
      case 501:
        yield put({ type: actionsSnackBar.SET_ERR });
        break;
      default:
        break;
    }
  } catch (error) {
    // continue
  }
}

export function* modifyUserFlow() {
  while (true) {
    const params = yield take(actions.MODIFY_USER);
    yield race({ test: call(modifyUser, params) });
  }
}
