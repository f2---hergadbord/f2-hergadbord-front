const action = {
  LOGIN: "LOGIN",
  LOGOUT: "LOGOUT",
  RESET_PASSWORD: "RESET_PASSWORD",
  SETCO: "SETCO",
  UNSETCO: "UNSETCO",
  SIGNUP: "SIGNUP",
  MODIFY_USER: "MODIFY_USER",
  UPDATE_USER: "UPDATE_USER",
  GET_USER: "GET_USER",

  login: info => ({
    type: action.LOGIN,
    info
  }),
  logout: () => ({
    type: action.LOGOUT
  }),
  forgotPassword: info => ({
    type: action.RESET_PASSWORD,
    info
  }),
  signUp: info => ({
    type: action.SIGNUP,
    info
  }),
  modifyUser: info => ({
    type: action.MODIFY_USER,
    info
  }),
  getUser: info => ({
    type: action.GET_USER,
    info
  })
};
export default action;
