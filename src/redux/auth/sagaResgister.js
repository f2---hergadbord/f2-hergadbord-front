import { take, call, race, put } from "redux-saga/effects";
import actions from "./action";
import onGetJson from "../onGetJson";
import actionsSnackBar from "../snackBar/action";
import { users } from "../../api";
import history from "../../history";

function forwardTo(location) {
  history.push(location);
}

export function* SignUp(info) {
  const server = users;
  const body = {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      Accept: "application/json"
    },
    body: JSON.stringify({
      email: info.info.email,
      password: info.info.passwd,
      pseudo: info.info.pseudo,
      idAvatar: info.info.avatar
    })
  };
  try {
    const result = yield call(onGetJson, body, server);
    switch (result.status) {
      case 201:
        yield put({ type: actionsSnackBar.SET_OK_REGISTER });
        yield call(forwardTo, "/test");
        break;
      case 501:
        yield put({ type: actionsSnackBar.SET_ERR });
        break;
      default:
        break;
    }
  } catch (error) {
    // continue
  }
}

export function* signUpFlow() {
  while (true) {
    const params = yield take(actions.SIGNUP);
    yield race({ test: call(SignUp, params) });
  }
}
