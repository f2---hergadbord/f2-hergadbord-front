import { take, call, put, fork, race } from "redux-saga/effects";

import actions from "./action";
import { signUpFlow } from "./sagaResgister";
import { modifyUserFlow } from "./sagaModifyUser";
import { getUserFlow } from "./sagaGetUser";
import actionsSnackBar from "../snackBar/action";
import onGetJson from "../onGetJson";
import { usersLogin } from "../../api";
import actionRooms from "../rooms/action";
import actionUsers from "../users/action";

export function* login(info) {
  const server = usersLogin;
  const body = {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      Accept: "application/json"
    },
    body: JSON.stringify({
      email: info.info.email,
      password: info.info.passwd
    })
  };
  try {
    const result = yield call(onGetJson, body, server);
    switch (result.status) {
      case 200:
        yield put({
          type: actions.SETCO,
          payload: {
            token: result.body.token,
            email: info.info.email,
            idAvatar: result.body.user.idAvatar,
            pseudo: result.body.user.pseudo,
            id: result.body.user.id
          }
        });
        yield put({ type: actionsSnackBar.SET_OK_LOGIN });
        // history.push("/dashboard/Games");
        break;
      case 501:
        yield put({ type: actionsSnackBar.SET_ERR });
        break;
      case 404:
        yield put({ type: actionsSnackBar.SET_ERR_LOGIN });
        break;
      default:
        break;
    }
  } catch (error) {
    // continue
  }
}

export function* loginFlow() {
  while (true) {
    const params = yield take(actions.LOGIN);
    yield race({ test: call(login, params) });
  }
}

export function* logoutFlow() {
  while (true) {
    yield take(actions.LOGOUT);
    yield put({
      type: actionRooms.UNSETCO
    });

    yield put({
      type: actionUsers.UNSETCO
    });
    yield put({
      type: actions.UNSETCO
    });
  }
}

export default function* root() {
  yield fork(loginFlow);
  yield fork(logoutFlow);
  yield fork(signUpFlow);
  yield fork(modifyUserFlow);
  yield fork(getUserFlow);
}
