import { take, call, race, select } from "redux-saga/effects";
import actions from "./action";
import onGetJson from "../onGetJson";
import { users } from "../../api";

const getToken = state => state.Auth.token;

export function* getUser(info) {
  const token = yield select(getToken);
  const server = `${users}/${info.info.id}`;
  const authorization = `Bearer ${token}`;
  const body = {
    method: "GET",
    headers: {
      "Content-type": "application/json",
      Accept: "application/json",
      authorization
    }
  };
  try {
    const result = yield call(onGetJson, body, server);
    switch (result.status) {
      case 200:
        break;
      case 501:
        break;
      case 502:
        break;
      default:
        break;
    }
  } catch (error) {
    // contiiner
  }
}

export function* getUserFlow() {
  while (true) {
    const params = yield take(actions.GET_USER);
    yield race({ test: call(getUser, params) });
  }
}
