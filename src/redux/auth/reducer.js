import actions from "./action";
// import history from "../../history";

const initialState = {
  isLog: false,
  email: "",
  token: "",
  idAvatar: -1,
  pseudo: "",
  id: -1
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case actions.SETCO:
      return {
        ...state,
        isLog: true,
        token: action.payload.token,
        email: action.payload.email,
        idAvatar: action.payload.idAvatar,
        pseudo: action.payload.pseudo,
        id: action.payload.id
      };
    case actions.UPDATE_USER:
      return {
        ...state,
        email: action.payload.email,
        idAvatar: action.payload.idAvatar,
        pseudo: action.payload.pseudo,
        id: action.payload.id
      };
    case actions.UNSETCO:
      return {
        ...state,
        isLog: false,
        email: "",
        token: "",
        idAvatar: -1,
        pseudo: "",
        id: -1
      };
    default:
      return state;
  }
}

export default reducer;
