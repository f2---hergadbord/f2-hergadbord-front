import Auth from "./auth/reducer";
import SnackBar from "./snackBar/reducer";
import Users from "./users/reducer";
import Room from "./rooms/reducer";
import Socket from "./socket/reducer";

export default {
  Auth,
  SnackBar,
  Users,
  Room,
  Socket
};
