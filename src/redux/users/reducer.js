import actions from "./action";
// import history from "../../history";

const initialState = {
  allUsers: []
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case actions.SET_ALL_USER:
      return {
        ...state,
        allUsers: action.payload.users
      };

    case actions.UNSETCO:
      return {
        ...state,
        allUsers: []
      };
    default:
      return state;
  }
}

export default reducer;
