import { take, call, race, select, put } from "redux-saga/effects";

import actions from "./action";
import onGetJson from "../onGetJson";
import { users } from "../../api";

const getToken = state => state.Auth.token;

function addChecked(reponses) {
  reponses.map(response => Object.assign(response, { checked: false }));
}

export function* getAllUsers() {
  const token = yield select(getToken);
  const server = users;
  const authorization = `Bearer ${token}`;
  const body = {
    method: "GET",
    headers: {
      "Content-type": "application/json",
      Accept: "application/json",
      authorization
    }
  };
  try {
    const result = yield call(onGetJson, body, server);
    switch (result.status) {
      case 200:
        addChecked(result.body);
        yield put({
          type: actions.SET_ALL_USER,
          payload: {
            users: result.body
          }
        });
        break;
      default:
        break;
    }
  } catch (error) {
    // continue
  }
}

export function* getAllUsersFlow() {
  while (true) {
    yield take(actions.GET_ALL_USER);
    yield race({ test: call(getAllUsers) });
  }
}
