const action = {
  GET_ALL_USER: "GET_ALL_USER",
  SET_ALL_USER: "SET_ALL_USER",
  UNSETCO: "UNSETCO",

  getAllUsers: () => ({
    type: action.GET_ALL_USER
  })
};
export default action;
