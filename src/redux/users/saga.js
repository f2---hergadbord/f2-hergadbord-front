import { fork } from "redux-saga/effects";

import { getAllUsersFlow } from "./sagaGetAllUsers";

export default function* root() {
  yield fork(getAllUsersFlow);
}
