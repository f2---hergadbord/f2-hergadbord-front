import { all } from "redux-saga/effects";
import authSagas from "./auth/saga";
import snackBarSagas from "./snackBar/saga";
import usersSagas from "./users/saga";
import roomsSagas from "./rooms/saga";
import socketSagas from "./socket/saga";

export default function* rootSaga() {
  yield all([
    authSagas(),
    snackBarSagas(),
    usersSagas(),
    roomsSagas(),
    socketSagas()
  ]);
}
