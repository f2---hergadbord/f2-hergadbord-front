const onGetJson = async (rest, data) => {
  const response = await fetch(data, rest);
  const ret = {
    status: response.status,
    body: await response.json()
  };
  return ret;
};

export default onGetJson;
