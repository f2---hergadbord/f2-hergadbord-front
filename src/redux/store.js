import { createStore, applyMiddleware, compose } from "redux";
import { persistStore, persistCombineReducers } from "redux-persist";
// import { createActiveMiddleware } from "redux-active";
import localForage from "localforage";
import createSagaMiddleware from "redux-saga";
import thunk from "redux-thunk";
import rootSaga from "./sagas";
import reducers from "./reducers";

const sagaMiddleware = createSagaMiddleware();
// const activeMiddleware = createActiveMiddleware();

const middlewares = [thunk, sagaMiddleware];

const config = {
  key: "root",
  storage: localForage,
  blacklist: ["Chat"]
};

const reducer = persistCombineReducers(config, reducers);

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  reducer,
  composeEnhancers(applyMiddleware(...middlewares))
);

const persistor = persistStore(store);

sagaMiddleware.run(rootSaga);

export { store, persistor };
