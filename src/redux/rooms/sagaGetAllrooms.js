import { take, call, race, select, put } from "redux-saga/effects";

import actions from "./action";
import onGetJson from "../onGetJson";
import { roomsUser } from "../../api";

const getToken = state => state.Auth.token;

export function* getAllRooms() {
  const token = yield select(getToken);
  const server = roomsUser;
  const authorization = `Bearer ${token}`;
  const body = {
    method: "GET",
    headers: {
      "Content-type": "application/json",
      Accept: "application/json",
      authorization
    }
  };
  try {
    const result = yield call(onGetJson, body, server);
    switch (result.status) {
      case 200:
        yield put({
          type: actions.SET_ALL_ROOMS,
          payload: result.body
        });
        break;
      case 404:
        yield put({
          type: actions.SET_ALL_ROOMS,
          payload: []
        });
        break;
      default:
        break;
    }
  } catch (error) {
    // continue
  }
}

export function* getAllRoomsFlow() {
  while (true) {
    const params = yield take(actions.GET_ALL_ROOMS);
    yield race({ test: call(getAllRooms, params) });
  }
}
