import { fork } from "redux-saga/effects";

import { createRoomFlow } from "./sagaCreateRoom";
import { getAllRoomsFlow } from "./sagaGetAllrooms";
import { deleteRoomFlow } from "./sagaDeleteRoom";

export default function* root() {
  yield fork(createRoomFlow);
  yield fork(getAllRoomsFlow);
  yield fork(deleteRoomFlow);
}
