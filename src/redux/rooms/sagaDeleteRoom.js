import { take, call, race, select, put } from "redux-saga/effects";
import actions from "./action";
import onGetJson from "../onGetJson";
import actionsSnackBar from "../snackBar/action";
import { rooms } from "../../api";

const getToken = state => state.Auth.token;

export function* deleteRoom(info) {
  const token = yield select(getToken);
  const server = `${rooms}/${info.info.id}`;
  const authorization = `Bearer ${token}`;
  const body = {
    method: "DELETE",
    headers: {
      "Content-type": "application/json",
      Accept: "application/json",
      authorization
    }
  };
  try {
    const result = yield call(onGetJson, body, server);
    switch (result.status) {
      case 200:
        yield put({ type: actionsSnackBar.SET_OK_DELETE_ROOM });
        yield put({ type: actions.GET_ALL_ROOMS });
        break;
      default:
        break;
    }
  } catch (error) {
    // continue
  }
}

export function* deleteRoomFlow() {
  while (true) {
    const params = yield take(actions.DELETE_ROOM);
    yield race({ test: call(deleteRoom, params) });
  }
}
