import actions from "./action";

const initialState = {
  allRooms: []
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case actions.SET_ALL_ROOMS:
      return {
        ...state,
        allRooms: action.payload
      };

    case actions.UNSETCO:
      return {
        ...state,
        allRooms: []
      };
    default:
      return state;
  }
}

export default reducer;
