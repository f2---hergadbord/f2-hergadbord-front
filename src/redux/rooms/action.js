const action = {
  CREATE_ROOM: "CREATE_ROOM",
  GET_ALL_ROOMS: "GET_ALL_ROOMS",
  SET_ALL_ROOMS: "SET_ALL_ROOMS",
  GET_ONE_ROOM: "GET_ROOM",
  DELETE_ROOM: "DELETE_ROOM",
  PUT_MESSAGE: "PUT_MESSAGE",
  UNSETCO: "UNSETCO",
  //  SET_ONE_ROOM: "SET_ROOM",

  createRoom: info => ({
    type: action.CREATE_ROOM,
    info
  }),
  getAllRooms: () => ({
    type: action.GET_ALL_ROOMS
  }),
  deleteRoom: info => ({
    type: action.DELETE_ROOM,
    info
  })
};
export default action;
