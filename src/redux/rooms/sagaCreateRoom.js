import { take, call, race, select, put } from "redux-saga/effects";
import actions from "./action";
import onGetJson from "../onGetJson";
import actionsSnackBar from "../snackBar/action";
import history from "../../history";
import { rooms } from "../../api";

const getToken = state => state.Auth.token;

export function* createRoom(info) {
  const token = yield select(getToken);
  const server = rooms;
  const authorization = `Bearer ${token}`;
  const body = {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      Accept: "application/json",
      authorization
    },
    body: JSON.stringify({
      adminIds: info.info.adminIds,
      userIds: info.info.userIds,
      name: info.info.name
    })
  };
  try {
    const result = yield call(onGetJson, body, server);
    switch (result.status) {
      case 200:
        yield put({ type: actionsSnackBar.SET_OK_CREATE_ROOM });
        history.push("/dashboard");
        break;
      default:
        break;
    }
  } catch (error) {
    // continue
  }
}

export function* createRoomFlow() {
  while (true) {
    const params = yield take(actions.CREATE_ROOM);
    yield race({ test: call(createRoom, params) });
  }
}
