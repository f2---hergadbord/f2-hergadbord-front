import actions from "./action";

const initialState = {
  open: false,
  messageFR: "",
  messageEN: "",
  type: ""
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case actions.SET_ERR_LOGIN:
      return {
        ...state,
        open: true,
        messageFR:
          "Aucun utilisateur ne correspond à ces identifiants, reessayer.",
        messageEN: "No user matches these credentials, try again.",
        type: "error"
      };
    case actions.SET_OK_LOGIN:
      return {
        ...state,
        open: true,
        messageFR: "Vous êtes connecté !",
        messageEN: "You're connected !",
        type: "succes"
      };

    case actions.SET_OK_REGISTER:
      return {
        ...state,
        open: true,
        messageFR:
          "Vous inscription est faite ! Veuillez maintenat vous connecter !",
        messageEN: "You registration is done! Please login now!",
        type: "succes"
      };

    case actions.SET_OK_CREATE_ROOM:
      return {
        ...state,
        open: true,
        messageFR: "La conversation a bien été créée",
        messageEN: "The conversation has been created",
        type: "succes"
      };
    case actions.SET_OK_DELETE_ROOM:
      return {
        ...state,
        open: true,
        messageFR: "La conversation a bien été suprimé",
        messageEN: "The conversation has been deleted",
        type: "succes"
      };
    case actions.SET_OK_MODIFY_USER:
      return {
        ...state,
        open: true,
        messageFR: "L'utilisateur a bien été modifié",
        messageEN: "The user has been modified",
        type: "succes"
      };
    case actions.SOMEONE_JOIN:
      return {
        ...state,
        open: true,
        messageFR: `${action.payload} c'est connecté a la conversation`,
        messageEN: `${action.payload} is now connected`,
        type: "info"
      };
    case actions.SOMEONE_LEAVE:
      return {
        ...state,
        open: true,
        messageFR: `${action.payload} est maintenant déconnecté`,
        messageEN: `${action.payload} is now disconnected`,
        type: "info"
      };

    case actions.WHO_IS_ALREADY_HERE:
      return {
        ...state,
        open: true,
        messageFR: `${action.payload} sont également connecté`,
        messageEN: `${action.payload} are already connected`,
        type: "info"
      };

    case actions.RESET:
      return {
        ...state,
        open: false,
        messageFR: "",
        messageEN: "",
        type: ""
      };
    default:
      return state;
  }
}

export default reducer;
