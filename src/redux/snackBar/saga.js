import { take, call, put, fork, race } from "redux-saga/effects";
import actions from "./action";

export function* reset() {
  yield put({
    type: actions.RESET
  });
}

export function* resetSnackBar() {
  while (true) {
    yield take(actions.RESET);
    yield race({ test: call(reset) });
  }
}

export default function* root() {
  yield fork(resetSnackBar);
}
