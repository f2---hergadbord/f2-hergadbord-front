const action = {
  CREATE_CONNECTION: "CREATE_CONNECTION",
  CLOSE_CONNECTION: "CLOSE_CONNECTION",
  SET_SOCKET: "SET_SOCKET",
  SEND_MESSAGE: "SEND_MESSAGE",
  SET_UNCO: "SET_UNCO",
  SET_CO: "SET_CO",

  createConnection: info => ({
    type: action.CREATE_CONNECTION,
    info
  }),
  closeConnection: () => ({
    type: action.CLOSE_CONNECTION
  }),
  sendMessage: info => ({
    type: action.SEND_MESSAGE,
    info
  })
};
export default action;
