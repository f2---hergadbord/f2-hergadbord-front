import actions from "./action";

const initialState = {
  socket: false
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case actions.SET_CO:
      return {
        ...state,
        socket: true
      };
    case actions.SET_UNCO:
      return {
        ...state,
        socket: false
      };
    default:
      return state;
  }
}

export default reducer;
