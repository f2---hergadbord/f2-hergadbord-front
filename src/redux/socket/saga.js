import { fork } from "redux-saga/effects";

import { createConnectionFlow } from "./sagaCreateConnection";

export default function* root() {
  yield fork(createConnectionFlow);
}
