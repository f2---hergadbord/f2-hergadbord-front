import { take, call, select, put, fork, cancel } from "redux-saga/effects";
import io from "socket.io-client";
import { eventChannel } from "redux-saga";
import history from "../../history";
import actions from "./action";
import actionRooms from "../rooms/action";
import { server } from "../../api";
import actionsSnackBar from "../snackBar/action";

const getToken = state => state.Auth.token;
const getRooms = state => state.Room.allRooms;
const getIdUser = state => state.Auth.id;
const getUsers = state => state.Users.allUsers;

let o = -2;

function createConnection(info, token) {
  const options = {
    transports: ["websocket"],
    upgrade: false,
    query: {
      token,
      room: info.info.selectedRoom
    }
  };
  const socket = io(server, options);
  return socket;
}

function subscribe(socket) {
  return eventChannel(emit => {
    socket.on("connect", () => {
      emit({ type: "connect" });
    });
    socket.on("users", obj => {
      emit({ type: "users", users: obj.users });
    });
    socket.on("message", obj => {
      emit({ type: "message", message: obj.message, id: obj.id });
    });
    socket.on("join", obj => {
      emit({ type: "join", pseudo: obj.pseudo, id: obj.id });
    });
    socket.on("leave", obj => {
      emit({ type: "leave", pseudo: obj.pseudo, id: obj.id });
    });
    socket.on("disconnect", () => {
      emit({ type: "disconnect" });
    });
    return () => {};
  });
}

function* read(socket, info) {
  const rooms = yield select(getRooms);
  const users = yield select(getUsers);
  const channel = yield call(subscribe, socket);

  while (true) {
    const response = yield take(channel);
    let pseudos = "";

    switch (response.type) {
      case "connect":
        yield put({ type: actions.SET_CO });
        break;
      case "message":
        for (let i = 0; i < rooms.length; i += 1) {
          if (rooms[i].id === info.info.selectedRoom) {
            rooms[i].message.push({
              message: response.message,
              userId: response.id,
              id: o
            });
            o -= 1;
          }
        }
        yield put({ type: actionRooms.SET_ALL_ROOMS, payload: [...rooms] });

        break;
      case "join":
        yield put({
          type: actionsSnackBar.SOMEONE_JOIN,
          payload: response.pseudo
        });
        break;
      case "leave":
        yield put({
          type: actionsSnackBar.SOMEONE_LEAVE,
          payload: response.pseudo
        });
        break;
      case "disconnect":
        yield put({ type: actions.SET_UNCO });
        socket.close();
        history.push("/");
        return;
      case "users":
        for (let i = 0; i < response.users.length; i += 1) {
          for (let y = 0; y < users.length; y += 1) {
            if (response.users[i] === users[y].id)
              pseudos += `${users[y].pseudo} `;
          }
        }
        if (pseudos !== "") {
          yield put({
            type: actionsSnackBar.WHO_IS_ALREADY_HERE,
            payload: pseudos
          });
        }
        break;
      default:
        break;
    }
  }
}

function* write(socket, infoRoom) {
  while (true) {
    const info = yield take(actions.SEND_MESSAGE);
    const rooms = yield select(getRooms);
    const rooms2 = [...rooms];
    const idUser = yield select(getIdUser);

    for (let i = 0; i < rooms2.length; i += 1) {
      if (rooms2[i].id === infoRoom.info.selectedRoom) {
        rooms2[i].message.push({
          message: info.info.message,
          userId: idUser,
          id: o
        });
        o -= 1;
      }
    }
    yield put({ type: actionRooms.SET_ALL_ROOMS, payload: rooms2 });
    socket.emit("message", info.info.message);
  }
}

export function* createConnectionFlow() {
  while (true) {
    const params = yield take(actions.CREATE_CONNECTION);
    const token = yield select(getToken);
    const socket = yield call(createConnection, params, token);
    const readF = yield fork(read, socket, params);
    const writeF = yield fork(write, socket, params);
    yield take(actions.CLOSE_CONNECTION);
    yield cancel(readF);
    yield cancel(writeF);
    socket.close();
    yield put({ type: actions.SET_UNCO });
  }
}

export function test1() {
  return "test1";
}
