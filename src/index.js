import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import { Provider } from "react-redux";
import { Router } from "react-router-dom";

import { PersistGate } from "redux-persist/lib/integration/react";
import { store, persistor } from "./redux/store";
import * as serviceWorker from "./serviceWorker";
import history from "./history";
import AppRoutes from "./router";
import { I18nContextProvider } from "./language/translate";
import SnackBar from "./components/snackBar";

const dotenv = require("dotenv");

dotenv.config();
const app = (
  <I18nContextProvider>
    <Provider store={store}>
      <Router history={history}>
        <PersistGate persistor={persistor} loading={null}>
          <SnackBar />
          <AppRoutes />
        </PersistGate>
      </Router>
    </Provider>
  </I18nContextProvider>
);

ReactDOM.render(app, document.getElementById("root"));
serviceWorker.unregister();
