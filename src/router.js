import React from "react";
import { withRouter, Route, Redirect, Switch } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import NotFound from "./containers/notFound";
import BaseApp from "./containers/baseApp";
import Connection from "./containers/login";
// import Loading from "./components/loading";

const AppRoutes = ({ isLog }) => {
  return (
    <Switch>
      <Route
        exact
        path="/"
        render={() => (isLog ? <Redirect to="/dashboard" /> : <Connection />)}
      />
      <Route
        path="/dashboard"
        render={() => (isLog ? <BaseApp /> : <Redirect to="/" />)}
      />
      <Route
        path="/settings"
        render={() => (isLog ? <BaseApp /> : <Redirect to="/" />)}
      />
      <Route
        path="/about"
        render={() => (isLog ? <BaseApp /> : <Redirect to="/" />)}
      />

      <Route render={() => (isLog ? <NotFound /> : <Redirect to="/" />)} />
    </Switch>
  );
};

export default withRouter(
  connect(state => ({
    isLog: state.Auth.isLog
  }))(AppRoutes)
);

AppRoutes.propTypes = {
  isLog: PropTypes.bool
};
AppRoutes.defaultProps = { isLog: false };
