import React, { useContext } from "react";
import { connect } from "react-redux";

import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";

import { I18nContext } from "../../language/translate";
import Style from "./style";

function About() {
  const classes = Style();
  const { translate } = useContext(I18nContext);

  return (
    <div>
      <Paper className={classes.main}>
        <Typography className={classes.title} variant="h5" component="h3">
          {translate("definition_of_done_title")}
        </Typography>
        <Typography className={classes.text} component="p">
          {translate("definition_of_done_text1")}
          <br />
          <br />
          {translate("definition_of_done_text2")}
          <br />
          <br />
          {translate("definition_of_done_text3")}
        </Typography>
      </Paper>
    </div>
  );
}

const mapStateToProps = state => state;

export default connect(mapStateToProps)(About);
