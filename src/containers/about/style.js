import { makeStyles } from "@material-ui/styles";

const Style = makeStyles({
  main: {
    margin: "60px",
    padding: "50px",
    display: "block",
    position: "absolute",
    height: "auto",
    bottom: 0,
    top: 0,
    left: 0,
    right: 0,
    overflow: "scroll",
    // background: "rgb(234, 234, 234)",
    background: "rgba(234, 234, 234, 0.3) !important",
    "@media (max-width:887px)": {
      position: "unset",
      height: "500px"
    }
  },
  title: {
    lineHeight: "3.33 !important",
    "@media (max-width:887px)": {
      lineHeight: "1.8 !important"
    }
  },
  text: {
    textAlign: "justify"
  }
});

export default Style;
