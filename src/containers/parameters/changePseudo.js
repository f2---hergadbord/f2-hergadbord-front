import React, { useContext, useState } from "react";
import { connect, useDispatch } from "react-redux";
import PropTypes from "prop-types";

import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import Fab from "@material-ui/core/Fab";

import action from "../../redux/auth/action";
import { I18nContext } from "../../language/translate";
import Style from "./style";
import history from "../../history";

import { getDisablePseudo, catchErr } from "../login/inputCatchErr";

const { modifyUser } = action;

function ChangePseudo({ idAvatar, id, email, oldPseudo }) {
  const classes = Style();
  const dispatch = useDispatch();
  const { translate } = useContext(I18nContext);
  const [pseudo, setPseudo] = useState("");

  const handleChangePseudo = event => setPseudo(event.target.value);

  function handlePush(event) {
    event.preventDefault();
    dispatch(modifyUser({ idAvatar, id, email, pseudo }));
  }
  const back = () => history.push("/settings");

  return (
    <div className={classes.mainPassword}>
      <Fab
        className={classes.back}
        size="small"
        color="primary"
        aria-label="Add"
      >
        <ArrowBackIcon onClick={() => back()} />
      </Fab>
      <div className={classes.changePassword}>
        <form
          className={classes.form}
          noValidate
          autoComplete="off"
          onSubmit={handlePush}
        >
          <FormControl
            error={catchErr("pseudo", pseudo)}
            margin="normal"
            required
            fullWidth
          >
            <InputLabel htmlFor="new_pseudo">
              {translate("new_pseudo")}
            </InputLabel>
            <Input
              name="new_pseudo"
              type="string"
              id="new_pseudo"
              autoComplete="new_pseudo"
              onChange={e => handleChangePseudo(e)}
            />
          </FormControl>

          <Button
            disabled={getDisablePseudo(pseudo, oldPseudo)}
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.button}
          >
            {translate("change_pseudo")}
          </Button>
        </form>
      </div>
    </div>
  );
}

ChangePseudo.propTypes = {
  idAvatar: PropTypes.number.isRequired,
  id: PropTypes.number.isRequired,
  email: PropTypes.string.isRequired,
  oldPseudo: PropTypes.string.isRequired
};

const mapStateToProps = state => ({
  idAvatar: state.Auth.idAvatar,
  id: state.Auth.id,
  email: state.Auth.email,
  oldPseudo: state.Auth.pseudo
});

export default connect(mapStateToProps)(ChangePseudo);
