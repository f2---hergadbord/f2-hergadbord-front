import React, { useContext } from "react";
import { connect } from "react-redux";
import { useRouteMatch, Link } from "react-router-dom";

import { Button } from "@material-ui/core";

import { I18nContext } from "../../language/translate";
import Style from "./style";

function ListOfParams() {
  const classes = Style();
  const { translate } = useContext(I18nContext);
  const { url } = useRouteMatch();

  return (
    <div>
      <ul className={classes.listButton}>
        <li>
          <Link className={classes.link} to={`${url}/avatar`}>
            <Button
              fullWidth
              variant="contained"
              color="primary"
              className={classes.button1}
            >
              {translate("change_avatar")}
            </Button>
          </Link>
        </li>
        <li>
          <Link className={classes.link} to={`${url}/pseudo`}>
            <Button
              fullWidth
              variant="contained"
              color="primary"
              className={classes.button1}
            >
              {translate("change_pseudo")}
            </Button>
          </Link>
        </li>
        <li>
          <Link className={classes.link} to={`${url}/password`}>
            <Button
              fullWidth
              variant="contained"
              color="primary"
              className={classes.button1}
            >
              {translate("change_password")}
            </Button>
          </Link>
        </li>
      </ul>
    </div>
  );
}

const mapStateToProps = state => state;

export default connect(mapStateToProps)(ListOfParams);
