import React, { useContext } from "react";
import { connect } from "react-redux";
import { withRouter, Route, Switch, useRouteMatch } from "react-router-dom";

import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";

import { I18nContext } from "../../language/translate";
import Style from "./style";
import ListOfParams from "./listOfParams";
import ChangeAvatar from "./changeAvatar";
import ChangePassword from "./changePassword";
import ChangePseudo from "./changePseudo";

const Routes = path => {
  return (
    <Switch>
      <Route exact path={path} component={ListOfParams} />
      <Route exact path={`${path}/avatar`} component={ChangeAvatar} />
      <Route exact path={`${path}/pseudo`} component={ChangePseudo} />
      <Route exact path={`${path}/password`} component={ChangePassword} />
    </Switch>
  );
};

function Parameters() {
  const classes = Style();
  const { translate } = useContext(I18nContext);
  const { path } = useRouteMatch();

  return (
    <div>
      <Paper className={classes.main}>
        <Typography className={classes.title} variant="h5" component="h3">
          {translate("settings")}
        </Typography>
        {Routes(path)}
      </Paper>
    </div>
  );
}

export default withRouter(connect(state => state)(Parameters));
