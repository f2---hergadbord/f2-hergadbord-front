import React, { useContext, useState } from "react";
import { connect, useDispatch } from "react-redux";
import PropTypes from "prop-types";

import Button from "@material-ui/core/Button";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Fab from "@material-ui/core/Fab";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";

import history from "../../history";
import action from "../../redux/auth/action";
import { I18nContext } from "../../language/translate";
import Style from "./style";
import { client } from "../../api";

const { modifyUser } = action;

function ChangeAvatar({ oldAvatar, id, email, pseudo }) {
  const classes = Style();
  const { translate } = useContext(I18nContext);
  const dispatch = useDispatch();
  const [idAvatar, setIdAvatar] = useState(oldAvatar);

  const numbers = [];
  for (let i = 1; i < 35; i += 1) {
    numbers.push(i);
  }

  const handleChangeAvatar = number =>
    idAvatar === number ? setIdAvatar(-1) : setIdAvatar(number);
  const back = () => history.push("/settings");

  function getAvatar(number) {
    if (number === idAvatar) return true;
    return false;
  }

  const test = number => (
    <Checkbox
      checked={getAvatar(number)}
      onChange={() => handleChangeAvatar(number)}
    />
  );
  const listAvatar = numbers.map(number => (
    <Grid
      key={number}
      xs={12}
      sm={6}
      md={3}
      item
      className={classes.avatarEnfant}
    >
      <div className={classes.avatar1par}>
        <img
          className={classes.avatar1}
          src={`${client}/files/avatar/${number}.png`}
          alt="choix avatar"
        />
      </div>
      <FormControlLabel
        className={classes.imagesCheck}
        control={test(number)}
      />
    </Grid>
  ));

  function handlePush(event) {
    event.preventDefault();
    dispatch(modifyUser({ idAvatar, id, email, pseudo }));
  }

  return (
    <div className={classes.mainAvatar}>
      <Fab
        className={classes.back}
        size="small"
        color="primary"
        aria-label="Add"
      >
        <ArrowBackIcon onClick={() => back()} />
      </Fab>
      <Typography className={classes.titleAvatar} variant="h6">
        {translate("select_new_avatar")}
      </Typography>
      <form
        className={classes.form}
        noValidate
        autoComplete="off"
        onSubmit={handlePush}
      >
        <Grid spacing={24} container className={classes.jesuistest}>
          {listAvatar}
        </Grid>
        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
          className={classes.button}
          disabled={idAvatar === -1 || idAvatar === oldAvatar}
        >
          {translate("change_avatar")}
        </Button>
      </form>
    </div>
  );
}

ChangeAvatar.propTypes = {
  oldAvatar: PropTypes.number.isRequired,
  id: PropTypes.number.isRequired,
  email: PropTypes.string.isRequired,
  pseudo: PropTypes.string.isRequired
};

const mapStateToProps = state => ({
  oldAvatar: state.Auth.idAvatar,
  id: state.Auth.id,
  email: state.Auth.email,
  pseudo: state.Auth.pseudo
});

export default connect(mapStateToProps)(ChangeAvatar);
