import React, { useContext, useState } from "react";
import { connect, useDispatch } from "react-redux";
import PropTypes from "prop-types";

import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import Fab from "@material-ui/core/Fab";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";

import history from "../../history";
import action from "../../redux/auth/action";
import { I18nContext } from "../../language/translate";
import Style from "./style";
import { getDisablePassword, catchErr } from "../login/inputCatchErr";

const { modifyUser } = action;

function ChangePassword({ idAvatar, id, email, pseudo }) {
  const classes = Style();
  const dispatch = useDispatch();
  const { translate } = useContext(I18nContext);
  const [password, setPassword] = useState("");

  const handleChangePassword = event => setPassword(event.target.value);
  const back = () => history.push("/settings");

  function handlePush(event) {
    event.preventDefault();
    dispatch(modifyUser({ idAvatar, id, email, pseudo, password }));
  }

  return (
    <div className={classes.mainPassword}>
      <Fab
        className={classes.back}
        size="small"
        color="primary"
        aria-label="Add"
      >
        <ArrowBackIcon onClick={() => back()} />
      </Fab>
      <div className={classes.changePassword}>
        <form
          className={classes.form}
          noValidate
          autoComplete="off"
          onSubmit={handlePush}
        >
          <FormControl
            error={catchErr("password", password)}
            margin="normal"
            required
            fullWidth
          >
            <InputLabel htmlFor="new_password">
              {translate("new_password")}
            </InputLabel>
            <Input
              name="new_password"
              type="password"
              id="new_password"
              autoComplete="new_password"
              onChange={e => handleChangePassword(e)}
            />
          </FormControl>

          <Button
            disabled={getDisablePassword(password)}
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.button}
          >
            {translate("change_password")}
          </Button>
        </form>
      </div>
    </div>
  );
}

ChangePassword.propTypes = {
  idAvatar: PropTypes.number.isRequired,
  id: PropTypes.number.isRequired,
  email: PropTypes.string.isRequired,
  pseudo: PropTypes.string.isRequired
};

const mapStateToProps = state => ({
  idAvatar: state.Auth.idAvatar,
  id: state.Auth.id,
  email: state.Auth.email,
  pseudo: state.Auth.pseudo
});

export default connect(mapStateToProps)(ChangePassword);
