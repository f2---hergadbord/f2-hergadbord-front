import { makeStyles } from "@material-ui/styles";

const Style = makeStyles({
  main: {
    margin: "60px",
    padding: "50px",
    display: "block",
    position: "absolute",
    height: "auto",
    bottom: 0,
    top: 0,
    left: 0,
    right: 0,
    // background: "rgb(234, 234, 234)",
    background: "rgba(234, 234, 234, 0.3) !important",
    "@media (max-width:887px)": {
      position: "unset",
      padding: "20px"
    }
  },
  title: {
    marginTop: "30px"
  },
  text: {
    textAlign: "justify"
  },
  listButton: {
    listStyle: "none",
    paddingInlineStart: 0
  },
  button1: {
    color: "white",
    width: "50% !important",
    marginTop: "30px !important",
    marginLeft: "auto !important",
    marginRight: "auto !important",
    display: "block !important",
    "@media (max-width:887px)": {
      width: "70% !important"
    }
  },
  link: {
    color: "white !important",
    textDecoration: "none !important",
    width: "100%"
  },
  avatarParent: {
    overflow: "scroll !important",
    maxHeight: "250px !important",
    width: "100%",
    display: "block !important",
    marginTop: "20px !important",
    marginBottom: "12px !important",
    paddingTop: "10px !important",
    paddingBottom: "10px !important"
  },
  avatarEnfant: {
    display: "inline-block !important",
    width: "33% !important",
    margin: "0 !important"
  },
  avatar1: {
    maxWidth: "50px"
  },
  avatar1par: {
    width: "100%"
  },
  imagesCheck: {
    width: "100%",
    marginLeft: "unset !important",
    marginRight: "unset !important",
    display: "unset !important"
  },
  jesuistest: {
    maxHeight: "calc(100% - 115px)",
    overflow: "scroll",
    marginBottom: "40px !important",
    marginTop: "30px !important",
    textAlign: "center",
    "@media (max-width:887px)": {
      maxHeight: "400px"
    }
  },
  form: {
    height: "100%"
  },
  mainAvatar: {
    height: "100%"
  },
  titleAvatar: {
    height: "15px"
  },
  changePassword: {
    maxWidth: "400px",
    marginLeft: "auto",
    marginRight: "auto"
  },
  back: {
    position: "absolute !important",
    top: "46px !important",
    right: "34px !important",
    "@media (max-width:887px)": {
      position: "unset !important"
    }
  }
});

export default Style;
