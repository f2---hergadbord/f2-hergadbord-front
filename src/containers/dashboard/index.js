import React from "react";
import { connect } from "react-redux";
import { Route, Switch, useRouteMatch } from "react-router-dom";

import CreateConversation from "./createConversation";
import PageConversation from "./conversation/index";

const Routes = path => {
  return (
    <Switch>
      <Route exact path={path} component={PageConversation} />
      <Route exact path={`${path}/createRoom`} component={CreateConversation} />
      <Route exact path={`${path}/modifyRoom`} component={CreateConversation} />
    </Switch>
  );
};

function Dashboard() {
  const { path } = useRouteMatch();

  return <div>{Routes(path)}</div>;
}

const mapStateToProps = state => state;
export default connect(mapStateToProps)(Dashboard);
