import React, { useState, useEffect } from "react";
import { connect, useDispatch } from "react-redux";

import Conversation from "./conversation";
import ListConversation from "./listConversation";
import actionSocket from "../../../redux/socket/action";

const { createConnection, closeConnection } = actionSocket;

function PageConversation() {
  const [selectedRoom, setSelectedRoom] = useState(-1);
  const dispatch = useDispatch();

  useEffect(() => {
    if (selectedRoom !== -1) dispatch(createConnection({ selectedRoom }));
    return () => {
      dispatch(closeConnection());
    };
  }, [selectedRoom]);

  const changeSelecetedRoom = i => {
    setSelectedRoom(i);
  };

  return (
    <div>
      <ListConversation
        setFunc={changeSelecetedRoom}
        selectedRoom={selectedRoom}
      />
      <Conversation selectedRoom={selectedRoom} />
    </div>
  );
}

const mapStateToProps = () => ({});

export default connect(mapStateToProps)(PageConversation);
