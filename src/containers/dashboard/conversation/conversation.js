import React, { useContext, useState, useRef, useEffect } from "react";
import { connect, useDispatch } from "react-redux";
import PropTypes from "prop-types";
import clsx from "clsx";

import Menu from "@material-ui/core/Menu";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import SendIcon from "@material-ui/icons/Send";
import MenuItem from "@material-ui/core/MenuItem";
import TextField from "@material-ui/core/TextField";
import IconButton from "@material-ui/core/IconButton";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import SupervisorAccountIcon from "@material-ui/icons/SupervisorAccount";

import { I18nContext } from "../../../language/translate";
import Style from "./style";
import actionRoom from "../../../redux/rooms/action";
import actionSocket from "../../../redux/socket/action";
import { client } from "../../../api";

const options = ["Delete"];
const { deleteRoom } = actionRoom;
const { sendMessage } = actionSocket;

function AlertDetele({ openDel, changeDetele, selectedRoom }) {
  const { translate } = useContext(I18nContext);
  const dispatch = useDispatch();

  function handlePush(event) {
    event.preventDefault();
    dispatch(deleteRoom({ id: selectedRoom }));
  }

  return (
    <Dialog
      open={openDel}
      onClose={() => changeDetele(false)}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">
        {translate("title_sure_delete")}
      </DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {translate("sure_delete")}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button color="primary" onClick={() => changeDetele(false)}>
          Disagree
        </Button>
        <Button color="primary" autoFocus onClick={event => handlePush(event)}>
          Agree
        </Button>
      </DialogActions>
    </Dialog>
  );
}

function isAdmin(id, selectedRoom, allRooms) {
  for (let i = 0; i < allRooms.length; i += 1) {
    if (allRooms[i].id === selectedRoom) {
      for (let o = 0; o < allRooms[i].adminIds.length; o += 1) {
        if (allRooms[i].adminIds[o] === id) {
          return true;
        }
      }
    }
  }
  return false;
}

function whoIs(messa, idUser, classes) {
  if (messa.userId === idUser) return classes.me;
  return classes.him;
}

const getDisable = (classes, selectedRoom) => {
  if (selectedRoom === -1) return classes.disable;
  return classes.enable;
};

function isCo(socket) {
  if (socket === true) return "inputMessageBarreCo";
  return "inputMessageBarreDisco";
}

function Conversation({ selectedRoom, idUser, allRooms, allUsers, socket }) {
  const classes = Style();
  const { translate } = useContext(I18nContext);
  const [anchorEl, setAnchorEl] = useState(null);
  const [openDel, setOpenDel] = useState(false);
  const [message, setMessage] = useState("");
  const messagesEndRef = useRef(null);

  const dispatch = useDispatch();

  const open = Boolean(anchorEl);

  let room = {};
  room.message = [];

  for (let o = 0; o < allRooms.length; o += 1) {
    if (allRooms[o].id === selectedRoom) {
      room = allRooms[o];
    }
  }

  const scrollToBottom = () => {
    messagesEndRef.current.scrollIntoView({ behavior: "smooth" });
  };

  useEffect(scrollToBottom, [allRooms]);
  useEffect(() => {
    scrollToBottom();
  });

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };
  const changeDetele = parm => {
    setOpenDel(parm);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleChangeMessage = event => {
    if (event.target.value.length < 200) setMessage(event.target.value);
  };

  const handleSendMessage = e => {
    e.preventDefault();
    if (message !== "") dispatch(sendMessage({ message }));
    setMessage("");
  };

  const getAvatarById = id => {
    for (let i = 0; i < allUsers.length; i += 1) {
      if (allUsers[i].id === id && idUser !== allUsers[i].id)
        return allUsers[i].idAvatar;
    }
    return -1;
  };

  const getPseudoById = id => {
    for (let i = 0; i < allUsers.length; i += 1) {
      if (allUsers[i].id === id && idUser !== allUsers[i].id)
        return `${allUsers[i].pseudo}: `;
    }
    return "";
  };

  const getConv = room.message.map(messa => {
    return (
      <div key={messa.id} className={whoIs(messa, idUser, classes)}>
        {getAvatarById(messa.userId) !== -1 ? (
          <img
            className={classes.avatar}
            alt="avatar"
            src={`${client}/files/avatar/${getAvatarById(messa.userId)}.png`}
          />
        ) : (
          <p className={classes.none} />
        )}
        <p className={classes.sp}>
          {getPseudoById(messa.userId)}
          {messa.message}
        </p>
      </div>
    );
  });
  return (
    <div className={classes.conversation}>
      <div className={classes.mainConv}>
        {isAdmin(idUser, selectedRoom, allRooms) === true ? (
          <div>
            <IconButton
              className={classes.params}
              aria-label="more"
              aria-controls="long-menu"
              aria-haspopup="true"
              onClick={handleClick}
            >
              <MoreVertIcon />
            </IconButton>
            <SupervisorAccountIcon className={classes.admin} />
            <Menu
              id="long-menu"
              anchorEl={anchorEl}
              keepMounted
              open={open}
              onClose={handleClose}
              PaperProps={{
                style: {
                  maxHeight: 300,
                  width: 200
                }
              }}
            >
              {options.map(option => (
                <MenuItem
                  key={option}
                  selected={option === "Pyxis"}
                  onClick={() => changeDetele(true)}
                >
                  {option}
                </MenuItem>
              ))}
            </Menu>
          </div>
        ) : (
          <p className={classes.none} />
        )}

        <div className={getDisable(classes, selectedRoom)}>
          <div className={classes.messagesBlocParents}>
            <Paper className={classes.messages}>
              {room.message ? getConv : <p />}
              <div ref={messagesEndRef} />
            </Paper>
          </div>

          <form
            className={classes.inputMessage}
            onSubmit={e => handleSendMessage(e)}
          >
            <TextField
              id="outlined-with-placeholder"
              label="Message"
              className={classes.inputMessageBarre}
              placeholder={translate("your_message")}
              margin="normal"
              variant="outlined"
              value={message}
              InputProps={{
                endAdornment: (
                  <Button type="submit">
                    <SendIcon
                      className={clsx(classes[isCo(socket)])}
                      type="submit"
                    />
                  </Button>
                )
              }}
              onChange={e => handleChangeMessage(e)}
            />
          </form>
        </div>
      </div>
      <AlertDetele
        changeDetele={changeDetele}
        openDel={openDel}
        selectedRoom={selectedRoom}
      />
    </div>
  );
}

Conversation.propTypes = {
  selectedRoom: PropTypes.number.isRequired,
  idUser: PropTypes.number.isRequired,
  allRooms: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      adminsId: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.number])),
      usersId: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.number])),
      message: PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.number,
          message: PropTypes.string,
          userId: PropTypes.number
        })
      )
    })
  ).isRequired,
  allUsers: PropTypes.arrayOf(
    PropTypes.shape({
      pseudo: PropTypes.string,
      id: PropTypes.number,
      idAvatar: PropTypes.number,
      checked: PropTypes.bool
    })
  ).isRequired,
  socket: PropTypes.bool.isRequired
};

const mapStateToProps = state => ({
  idUser: state.Auth.id,
  allRooms: state.Room.allRooms,
  allUsers: state.Users.allUsers,
  socket: state.Socket.socket
});

export default connect(mapStateToProps)(Conversation);

AlertDetele.propTypes = {
  changeDetele: PropTypes.func.isRequired,
  openDel: PropTypes.bool.isRequired,
  selectedRoom: PropTypes.number.isRequired
};
