import { makeStyles } from "@material-ui/styles";

const Style = makeStyles({
  menu: {
    width: "230px",
    background: "rgba(234, 234, 234, 0.3) !important",
    height: "100%",
    overflow: "scroll",
    position: "fixed",
    "@media (max-width:887px)": {
      width: "100%",
      position: "unset",
      maxHeight: "400px",
      display: "block"
    }
  },
  listARoom: {
    lineHeight: "80px",
    boxShadow: "inset 0px 0px 2px 0px grey",
    height: "80px",
    textAlign: "center",
    marginTop: "auto",
    marginBottom: "auto",
    OTransitionProperty:
      "width,height,-o-transform,background,font-size,opacity",
    OTransitionDuration: "0.2s,0.2s,0.2s,0.2s,0.2s,0.2s",
    MozTransitionProperty:
      "width,height,-o-transform,background,font-size,opacity",
    MozTransitionDuration: "0.2s,0.2s,0.2s,0.2s,0.2s,0.2s",
    transitionProperty: "width,height,transform,background,font-size,opacity",
    transitionDuration: "0.2s,0.2s,0.2s,0.2s,0.2s,0.2s",
    "&:hover": {
      width: "100%",
      fontSize: "20px"
    },
    "&:focus": { outline: "0" }
  },
  nameRoom: {
    height: "100%",
    verticalAlign: "middle"
  },
  selectedRoom: {
    background: "rgba(234, 234, 234, 0.6) !important",
    height: "80px",
    textAlign: "center",
    marginTop: "auto",
    marginBottom: "auto",
    fontSize: "20px",
    lineHeight: "80px",
    "&:focus": { outline: "0" }
  },
  conversation: {
    width: "calc(100% - 230px)",
    background: "rgba(234, 234, 234, 0.3) !important",
    height: "100%",
    position: "absolute",
    right: 0,
    "&:focus": { outline: "0" },
    "@media (max-width:887px)": {
      width: "100%",
      position: "unset",
      height: "600px",
      display: "block"
    }
  },
  inputMessage: {
    width: "calc(100% - 480px)",
    position: "fixed",
    bottom: 0,
    paddingLeft: "10px",
    paddingRight: "10px",
    height: "80px",
    "@media (max-width:887px)": {
      width: "calc(100% - 20px)",
      position: "unset"
    }
  },
  inputMessageBarre: {
    width: "100%"
  },
  inputMessageBarreCo: {
    color: "aqua !important"
  },
  inputMessageBarreDisco: {
    color: "red !important"
  },
  messagesBlocParents: {
    padding: "40px",
    height: "calc(100% - 80px)"
  },
  messages: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    font: "16px/0.3 sans-serif",
    listStyleType: "none",
    margin: " 0 auto",
    padding: "8px",
    overflow: "auto"
  },
  mainConv: {
    height: "100%"
  },

  options: {
    textAlign: "center",
    height: "60px",
    lineHeight: "60px !important"
  },
  optionsAdd: {
    position: "absolute !important",
    right: "0px",
    top: "10px"
  },
  params: {
    position: "absolute !important",
    right: "0 !important"
  },
  none: {
    display: "none"
  },
  admin: {
    position: "absolute"
  },
  him: {
    backgroundColor: "#CCCCCC",
    borderRadius: "10px",
    marginTop: "5px",
    marginBottom: "5px",
    paddingLeft: "20px",
    paddingRight: "20px",
    lineHeight: 1.5,
    maxWidth: "88%"
  },
  me: {
    borderRadius: "10px",

    alignSelf: " flex-end;",
    backgroundColor: "rgb(0, 153, 255)",
    marginTop: "5px",
    marginBottom: "5px",
    paddingLeft: "20px",
    paddingRight: "20px",
    lineHeight: 1.5,
    maxWidth: "88%"
  },
  sp: {
    verticalAlign: "middle",
    display: "inline-block",
    width: " calc(100% - 38px)"
  },
  avatar: {
    display: "inline-block",
    width: "30px",
    verticalAlign: "middle",
    marginRight: "7px"
  },
  disable: {
    height: "calc(100% - 80px)",
    pointerEvents: "none",
    opacity: "0.4"
  },
  enable: {
    height: "calc(100% - 80px)"
  }
});

export default Style;
