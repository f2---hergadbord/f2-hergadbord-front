import React, { useContext, useEffect } from "react";
import { connect, useDispatch } from "react-redux";
import { useRouteMatch, Link } from "react-router-dom";
import PropTypes from "prop-types";

import Typography from "@material-ui/core/Typography";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import Fab from "@material-ui/core/Fab";

import { I18nContext } from "../../../language/translate";
import Style from "./style";
import actionRooms from "../../../redux/rooms/action";
import actionUsers from "../../../redux/users/action";

const { getAllRooms } = actionRooms;
const { getAllUsers } = actionUsers;

function ListConverstation({ allRooms, setFunc, selectedRoom }) {
  const classes = Style();
  const { translate } = useContext(I18nContext);
  const { url } = useRouteMatch();
  const dispatch = useDispatch();

  // const [selectedRoom, setSelectedRoom] = useState(0);

  useEffect(() => {
    dispatch(getAllRooms());
    dispatch(getAllUsers());
  }, []);

  const listRoom = allRooms.map(game => (
    <div
      key={game.id}
      role="button"
      onClick={() => setFunc(game.id)}
      onKeyDown={() => setFunc(selectedRoom + 1)}
      onKeyUp={() => setFunc(selectedRoom - 1)}
      tabIndex={game.id}
      className={
        game.id !== selectedRoom ? classes.listARoom : classes.selectedRoom
      }
    >
      {game.name}
    </div>
  ));

  return (
    <div className={classes.menu}>
      <Typography className={classes.options} variant="caption">
        {translate("discussion")}
      </Typography>
      <Link to={`${url}/createRoom`}>
        <Fab
          size="small"
          color="primary"
          aria-label="Add"
          className={classes.optionsAdd}
        >
          <AddCircleIcon size="small" />
        </Fab>
      </Link>
      {allRooms !== [] ? listRoom : <p />}
    </div>
  );
}

ListConverstation.propTypes = {
  allRooms: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      adminsId: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.number])),
      usersId: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.number])),
      message: PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.number,
          message: PropTypes.string,
          userId: PropTypes.number
        })
      )
    })
  ).isRequired,
  setFunc: PropTypes.func.isRequired,
  selectedRoom: PropTypes.number.isRequired
};

const mapStateToProps = state => ({
  allRooms: state.Room.allRooms
});

export default connect(mapStateToProps)(ListConverstation);

/*
allRooms: PropTypes.arrayOf(
    PropTypes.oneOfType([
      {
        id: PropTypes.number,
        adminsId: PropTypes.arrayOf(PropTypes.oneOfType(PropTypes.number)),
        usersId: PropTypes.arrayOf(PropTypes.oneOfType(PropTypes.number)),
        message: PropTypes.arrayOf({
          id: PropTypes.number,
          message: PropTypes.string
        })
      }
    ])
  ).isRequired,

  */
