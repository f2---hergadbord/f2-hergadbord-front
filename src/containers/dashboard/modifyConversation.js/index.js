import React, { useContext, useEffect, useState } from "react";
import { connect, useDispatch } from "react-redux";
import PropTypes from "prop-types";

import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import FormControl from "@material-ui/core/FormControl";
import Button from "@material-ui/core/Button";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import { MultiSelect } from "@progress/kendo-react-dropdowns";

import { I18nContext } from "../../../language/translate";
import Style from "./style";
import actionRoom from "../../../redux/rooms/action";
import actionUsers from "../../../redux/users/action";

const { createRoom } = actionRoom;
const { getAllUsers } = actionUsers;

function fromBrutToUsable(allUsers) {
  const ret = [];
  allUsers.map(response => ret.push(response.pseudo));
  return ret;
}

function getIdOfSelectedUsers(allUsers, selected) {
  const ret = [];

  for (let i = 0; i < allUsers.length; i += 1) {
    for (let j = 0; j < selected.length; j += 1) {
      if (allUsers[i].pseudo === selected[j]) ret.push(allUsers[i].id);
    }
  }
  return ret;
}

function ModifyConversation({ allUsers, id }) {
  const classes = Style();
  const { translate } = useContext(I18nContext);
  const dispatch = useDispatch();
  const [pays, setPays] = useState([]);
  const [pseudo, setPseudo] = useState("");

  const onChange = event => setPays(event.target.value);
  const handleChangePseudo = event => setPseudo(event.target.value);

  useEffect(() => {
    dispatch(getAllUsers());
  }, []);

  function handlePush(event) {
    event.preventDefault();
    const usersId = getIdOfSelectedUsers(allUsers, pays);
    const adminIds = [];
    adminIds.push(id);
    dispatch(createRoom({ userIds: usersId, name: pseudo, adminIds }));
  }

  return (
    <Paper className={classes.main}>
      <Typography className={classes.title} variant="h5">
        {translate("create_conv")}
      </Typography>
      <Typography className={classes.subTitle} variant="subtitle1">
        {translate("select_users")}
      </Typography>

      <MultiSelect
        onChange={e => onChange(e)}
        data={fromBrutToUsable(allUsers)}
        filterable
        style={{ width: "100%", zIndex: 20 }}
      />

      <form
        className={classes.form}
        noValidate
        autoComplete="off"
        onSubmit={handlePush}
      >
        <FormControl
          // error={catchErr("pseudo", pseudo)}
          margin="normal"
          required
          fullWidth
          className={classes.form}
        >
          <InputLabel htmlFor="pseudo">{translate("conv_name")}</InputLabel>
          <Input
            name="pseudo"
            type="pseudo"
            id="pseudo"
            autoComplete="pseudo"
            onChange={e => handleChangePseudo(e)}
          />
        </FormControl>
        <Button
          // disabled={getDisableForgot(email)}
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
          className={classes.button}
        >
          {translate("create_conv")}
        </Button>
      </form>
    </Paper>
  );
}

ModifyConversation.propTypes = {
  allUsers: PropTypes.arrayOf(
    PropTypes.shape({
      pseudo: PropTypes.string,
      id: PropTypes.number,
      idAvatar: PropTypes.number,
      checked: PropTypes.bool
    })
  ).isRequired,
  id: PropTypes.number.isRequired
};

const mapStateToProps = state => ({
  allUsers: state.Users.allUsers,
  id: state.Auth.id
});

export default connect(mapStateToProps)(ModifyConversation);
