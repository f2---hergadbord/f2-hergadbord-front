import { makeStyles } from "@material-ui/styles";

const top = 10;
const left = 50;

const Style = makeStyles(() => ({
  main: {
    margin: "60px",
    padding: "50px",
    display: "block",
    position: "absolute",
    height: "auto",
    bottom: 0,
    top: 0,
    left: 0,
    right: 0,
    // background: "rgb(234, 234, 234)",
    background: "rgba(234, 234, 234, 0.3) !important",
    "@media (max-width:887px)": {
      position: "unset",
      height: "500px"
    }
  },
  modal: {
    position: "absolute",
    width: "40%",
    minHeight: "1000px",
    backgroundColor: "white",
    padding: "40px",
    outline: "none",
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${left}%)`,
    borderRadius: "7px"
  },
  formControl: {
    maxHeight: "500px",
    overflow: "scroll",
    width: "100%"
  },
  users: {
    width: "100%"
  },

  root: {
    flexGrow: 1,
    height: 250,
    minWidth: 290
  },
  input: {
    display: "flex",
    padding: 0,
    height: "auto"
  },
  valueContainer: {
    display: "flex",
    flexWrap: "wrap",
    flex: 1,
    alignItems: "center",
    overflow: "hidden"
  },
  chip: {
    // margin: theme.spacing(0.5, 0.25)
  },
  chipFocused: {},
  noOptionsMessage: {
    // padding: theme.spacing(1, 2)
  },
  singleValue: {
    fontSize: 16
  },
  placeholder: {
    position: "absolute",
    left: 2,
    bottom: 6,
    fontSize: 16
  },
  paper: {
    position: "absolute",
    zIndex: 1,
    // marginTop: theme.spacing(1),
    left: 0,
    right: 0
  },
  divider: {
    // height: theme.spacing(2)
  },
  title: {
    marginTop: "10px !important"
  },
  subTitle: {
    marginTop: "30px !important"
  },
  form: {
    textAlign: "center",
    width: "100%"
  },
  button: {
    maxWidth: "400px",
    marginTop: "40px !important"
  }
}));

export default Style;
