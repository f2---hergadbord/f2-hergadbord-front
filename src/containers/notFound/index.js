import React from "react";
import { connect } from "react-redux";

import Style from "./style";

function NotFound() {
  const classes = Style();

  return <div className={classes.main}>404 not found</div>;
}

const mapStateToProps = state => state;
export default connect(mapStateToProps)(NotFound);
