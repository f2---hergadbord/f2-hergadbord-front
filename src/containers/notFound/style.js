import { makeStyles } from "@material-ui/styles";

const Style = makeStyles({
  main: {
    width: "100%",
    display: "block",
    marginTop: "5%",
    textAlign: "center"
  }
});

export default Style;
