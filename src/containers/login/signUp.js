import React, { useState, useContext } from "react";
import { useDispatch, connect } from "react-redux";

import Avatar from "@material-ui/core/Avatar";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import Lock from "@material-ui/icons/Assignment";
import Typography from "@material-ui/core/Typography";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";

import { I18nContext } from "../../language/translate";
import { getDisableSignUp, catchErr } from "./inputCatchErr";
import action from "../../redux/auth/action";
import Style from "./style";
import { client } from "../../api";

const { signUp } = action;

function SignUp() {
  const [email, setEmail] = useState("");
  const [passwd, setPasswd] = useState("");
  const [pseudo, setPseudo] = useState("");
  // const [openForgot] = useState(false);
  const classes = Style();
  const dispatch = useDispatch();
  const { translate } = useContext(I18nContext);
  const [avatar, setAvatar] = useState(-1);
  const numbers = [];

  for (let i = 1; i < 35; i += 1) {
    numbers.push(i);
  }
  function handlePush(event) {
    event.preventDefault();
    dispatch(signUp({ email, passwd, pseudo, avatar }));
  }

  const handleChangeEmail = event => setEmail(event.target.value);
  const handleChangePasswd = event => setPasswd(event.target.value);
  const handleChangePseudo = event => setPseudo(event.target.value);
  const handleChangeAvatar = number =>
    avatar === number ? setAvatar(-1) : setAvatar(number);

  // const handleCloseModalForgot = event => openForgot(event.target.value);
  // const handleOpenModalForgot = event => openForgot(event.target.value);

  function getAvatar(number) {
    if (number === avatar) return true;
    return false;
  }
  const test = number => (
    <Checkbox
      checked={getAvatar(number)}
      onChange={() => handleChangeAvatar(number)}
    />
  );
  const listAvatar = numbers.map(number => (
    <div key={number} className={classes.avatarEnfant}>
      <div className={classes.avatar1par}>
        <img
          className={classes.avatar1}
          src={`${client}/files/avatar/${number}.png`}
          alt="choix avatar"
        />
      </div>
      <FormControlLabel
        className={classes.imagesCheck}
        control={test(number)}
      />
    </div>
  ));

  return (
    <div className={classes.mainSub}>
      <Paper className={classes.paper}>
        <Avatar className={classes.avatar}>
          <Lock className={classes.icon} />
        </Avatar>
        <Typography component="h1" variant="h5">
          {translate("sign_up")}
        </Typography>

        <p>{translate("create_account")}</p>
        <form
          className={classes.form}
          noValidate
          autoComplete="off"
          onSubmit={handlePush}
        >
          <FormControl
            error={catchErr("email", email)}
            margin="normal"
            required
            fullWidth
          >
            <InputLabel htmlFor="email">{translate("email_adress")}</InputLabel>
            <Input
              //  error={this.checkEmail}
              id="email1"
              name="email"
              autoComplete="email"
              autoFocus
              onChange={e => handleChangeEmail(e)}
            />
          </FormControl>
          <FormControl
            margin="normal"
            error={catchErr("password", passwd)}
            required
            fullWidth
          >
            <InputLabel htmlFor="password">{translate("password")}</InputLabel>
            <Input
              name="password"
              type="password"
              id="password1"
              autoComplete="current-password"
              onChange={e => handleChangePasswd(e)}
            />
          </FormControl>

          <FormControl
            error={catchErr("pseudo", pseudo)}
            margin="normal"
            required
            fullWidth
          >
            <InputLabel htmlFor="pseudo">{translate("user_name")}</InputLabel>
            <Input
              name="pseudo"
              type="pseudo"
              id="pseudo"
              autoComplete="pseudo"
              onChange={e => handleChangePseudo(e)}
            />
          </FormControl>
          <span className={classes.spanAva} />
          <FormControl
            required
            // error={error}
            component="fieldset"
            className={classes.avatarParent}
          >
            {listAvatar}
          </FormControl>
          <span className={classes.spanAva} />
          <Typography className={classes.text} component="p">
            {translate("select_avatar")}
          </Typography>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.button}
            disabled={getDisableSignUp(email, passwd, pseudo, avatar)}
          >
            {translate("sign_up")}
          </Button>
        </form>
      </Paper>
    </div>
  );
}

const mapStateToProps = state => state;
export default connect(mapStateToProps)(SignUp);
