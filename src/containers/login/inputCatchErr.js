export function validateField(fieldName, value) {
  let valide = "";
  let emailValid = "";
  let passwordValid = "";
  let pseudoValid = "";
  switch (fieldName) {
    case "email":
      emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
      valide = emailValid ? "" : " is invalid";
      break;
    case "password":
      passwordValid = value.length >= 6 && value.length <= 20;
      valide = passwordValid ? "" : " is too short";
      break;
    case "pseudo":
      pseudoValid = value.length >= 3 && value.length <= 12;
      valide = pseudoValid ? "" : " is too short";
      break;
    case "avatar":
      pseudoValid = value > 0;
      valide = pseudoValid ? "" : " select an avatar";
      break;
    default:
      break;
  }
  return valide;
}

export function getDisableForgot(email) {
  if (validateField("email", email) === "") return false;
  return true;
}

export function getDisablePseudo(pseudo, oldPseudo) {
  if (validateField("pseudo", pseudo) === "" && pseudo !== oldPseudo)
    return false;
  return true;
}
export function getDisablePassword(password) {
  if (validateField("password", password) === "") return false;
  return true;
}

export function getDisable(email, passwd) {
  if (
    validateField("email", email) === "" &&
    validateField("password", passwd) === ""
  )
    return false;
  return true;
}

export function getDisableSignUp(email, passwd, pseudo, avatar) {
  if (
    validateField("email", email) === "" &&
    validateField("password", passwd) === "" &&
    validateField("pseudo", pseudo) === "" &&
    validateField("avatar", avatar) === ""
  )
    return false;
  return true;
}

export function catchErr(type, value) {
  if (value === "") return false;
  const ret = validateField(type, value);
  if (ret === "") return false;
  return true;
}
