import React, { useState, useContext } from "react";
import { connect } from "react-redux";

import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import Typography from "@material-ui/core/Typography";

import { I18nContext } from "../../language/translate";
import { getDisableForgot, catchErr } from "./inputCatchErr";
import Style from "./style";

function ForgotPassword() {
  const classes = Style();
  const { translate } = useContext(I18nContext);
  const [email, setEmail] = useState("");

  const handleChangeEmail = event => setEmail(event.target.value);

  function handlePush(event) {
    event.preventDefault();
    // console.log("je suos dans le push du forgot");
    // dispatch(login({ email, passwd }));
  }

  return (
    <div>
      <Typography className={classes.title} variant="h5" component="h3">
        {translate("forgot_pass")}
      </Typography>
      <Typography className={classes.text} component="p">
        {translate("forgot_no_probleme")}
      </Typography>
      <form
        className={classes.form}
        noValidate
        autoComplete="off"
        onSubmit={handlePush}
      >
        <FormControl
          error={catchErr("email", email)}
          margin="normal"
          required
          fullWidth
        >
          <InputLabel htmlFor="email">{translate("email_adress")}</InputLabel>
          <Input
            id="email"
            name="email"
            autoComplete="email"
            autoFocus
            onChange={e => handleChangeEmail(e)}
          />
        </FormControl>

        <Button
          disabled={getDisableForgot(email)}
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
          className={classes.button}
        >
          {translate("reset_pass")}
        </Button>
      </form>
    </div>
  );
}

const mapStateToProps = state => state;
export default connect(mapStateToProps)(ForgotPassword);
