import React from "react";
import { connect } from "react-redux";

import LanguageSelect from "../../language/languageSelector";

import LogIn from "./logIn";
import SignUp from "./signUp";

import Style from "./style";

function Connection() {
  const classes = Style();

  return (
    <div>
      <LanguageSelect />
      <div className={classes.main}>
        <LogIn />
        <SignUp />
      </div>
    </div>
  );
}

const mapStateToProps = state => state;
export default connect(mapStateToProps)(Connection);
