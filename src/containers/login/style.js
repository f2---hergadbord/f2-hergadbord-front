import { makeStyles } from "@material-ui/styles";

const top = 20;
const left = 50;

const Style = makeStyles(theme => ({
  main: {
    width: "100%",
    display: "block",
    marginTop: "5%",
    textAlign: "center"
  },
  mainSub: {
    width: "50%",
    display: "inline-block",
    verticalAlign: "top",
    "@media (max-width:887px)": {
      width: "100%",
      display: "block",
      marginBottom: "30px"
    }
  },
  paper: {
    maxWidth: "400px",
    textAlign: "center",
    marginRight: "auto",
    marginLeft: "auto",
    padding: "15px",
    "@media (max-width:587px)": {
      maxWidth: "300px"
    }
  },
  email: {
    width: "100%"
  },
  password: {
    width: "100%"
  },
  form: {
    width: "100%"
  },
  button: {
    marginTop: "10px !important",
    width: "100%",
    marginBottom: "10px"
  },
  icon: {
    fontSize: "26px"
  },
  avatar: {
    marginLeft: "auto",
    marginRight: "auto",
    margin: theme.spacing,
    backgroundColor: " aliceblue"
  },
  label: {
    marginTop: "15px",
    width: "100%"
  },
  modal: {
    position: "absolute",
    width: "20%",
    backgroundColor: "white",
    padding: "40px",
    outline: "none",
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${left}%)`,
    borderRadius: "7px"
  },
  title: {
    marginBottom: "15px !important"
  },
  text: {
    marginBottom: "13px !important"
  },
  avatarParent: {
    overflow: "scroll !important",
    maxHeight: "250px !important",
    width: "100%",
    display: "block !important",
    marginTop: "20px !important",
    marginBottom: "12px !important",
    paddingTop: "10px !important",
    paddingBottom: "10px !important"
  },
  avatarEnfant: {
    display: "inline-block !important",
    width: "33% !important",
    margin: "0 !important"
  },
  avatar1: {
    maxWidth: "50px"
  },
  avatar1par: {
    width: "100%"
  },
  imagesCheck: {
    width: "100%",
    marginLeft: "unset !important",
    marginRight: "unset !important",
    display: "unset !important"
  },
  spanAva: {
    height: "1px",
    display: "block",
    background:
      "-webkit-gradient(radial, 50% 50%, 0, 50% 50%, 152, from(#D3D3D3), to(rgba(0, 0, 255, 0)))",
    marginTop: "10px"
  }
}));

export default Style;
