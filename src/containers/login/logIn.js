import React, { useState, useContext } from "react";
import { useDispatch, connect } from "react-redux";

import Avatar from "@material-ui/core/Avatar";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import Lock from "@material-ui/icons/Lock";
import Typography from "@material-ui/core/Typography";
import Modal from "@material-ui/core/Modal";
import Fade from "@material-ui/core/Fade";

import { I18nContext } from "../../language/translate";
import { getDisable, catchErr } from "./inputCatchErr";
import ForgotPassword from "./forgotPassword";
import action from "../../redux/auth/action";
import Style from "./style";

const { login } = action;

function LogIn() {
  const [email, setEmail] = useState("");
  const [passwd, setPasswd] = useState("");
  const [openForgot, setOpenForgot] = useState(false);
  const classes = Style();
  const dispatch = useDispatch();
  const { translate } = useContext(I18nContext);

  function handlePush(event) {
    event.preventDefault();
    dispatch(login({ email, passwd }));
  }

  const handleChangeEmail = event => setEmail(event.target.value);
  const handleChangePasswd = event => setPasswd(event.target.value);

  const handleChangeModalForgot = () => setOpenForgot(!openForgot);

  return (
    <div className={classes.mainSub}>
      <Paper className={classes.paper}>
        <Avatar className={classes.avatar}>
          <Lock className={classes.icon} />
        </Avatar>
        <Typography component="h1" variant="h5">
          {translate("log_in")}
        </Typography>
        <p>{translate("connect_account")}</p>

        <form
          className={classes.form}
          noValidate
          autoComplete="off"
          onSubmit={handlePush}
        >
          <FormControl
            error={catchErr("email", email)}
            margin="normal"
            required
            fullWidth
          >
            <InputLabel htmlFor="email">{translate("email_adress")}</InputLabel>
            <Input
              id="email"
              name="email"
              autoComplete="email"
              autoFocus
              onChange={e => handleChangeEmail(e)}
            />
          </FormControl>
          <FormControl
            error={catchErr("password", passwd)}
            margin="normal"
            required
            fullWidth
          >
            <InputLabel htmlFor="password">{translate("password")}</InputLabel>
            <Input
              name="password"
              type="password"
              id="password"
              autoComplete="current-password"
              onChange={e => handleChangePasswd(e)}
            />
          </FormControl>
          <Button
            color="primary"
            className={classes.button}
            onClick={() => handleChangeModalForgot()}
          >
            {translate("forgot_password?")}
          </Button>
          <Button
            disabled={getDisable(email, passwd)}
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.button}
          >
            {translate("log_in")}
          </Button>
        </form>
      </Paper>
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={openForgot}
        onClose={() => handleChangeModalForgot()}
      >
        <Fade in={openForgot}>
          <div className={classes.modal}>
            <ForgotPassword />
          </div>
        </Fade>
      </Modal>
    </div>
  );
}

const mapStateToProps = state => state;
export default connect(mapStateToProps)(LogIn);
