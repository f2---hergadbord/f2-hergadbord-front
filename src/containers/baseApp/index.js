import React, { useState, useContext } from "react";
import { connect, useDispatch } from "react-redux";
import PropTypes from "prop-types";

import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Fab from "@material-ui/core/Fab";
import Exit from "@material-ui/icons/ExitToApp";
import Typography from "@material-ui/core/Typography";

import LanguageSelect from "../../language/languageSelector";
import AppRouter from "../../routerPrivate";
import history from "../../history";
import Style from "./style";
import action from "../../redux/auth/action";
import { I18nContext } from "../../language/translate";
import { client } from "../../api";

const { logout } = action;

function BaseApp({ idAvatar, pseudo }) {
  const [sideList1] = useState(["dashboard", "settings", "about"]);
  const classes = Style();
  const { translate } = useContext(I18nContext);
  const dispatch = useDispatch();

  function routeChange(event, destination) {
    history.push(`/${destination}`);
  }
  function logoutFunc(event) {
    event.preventDefault();
    dispatch(logout());
  }

  const sideList = (
    <div className={classes.list}>
      <img
        alt="avatar"
        className={classes.iconHome}
        src={`${client}/files/avatar/${idAvatar}.png`}
      />

      <Typography component="h1" variant="h5">
        {pseudo}
      </Typography>

      <List>
        {sideList1.map((text, index) => (
          <ListItem
            className={classes.myli}
            index={index}
            button
            onClick={e => routeChange(e, text)}
            key={text}
          >
            <ListItemText primary={translate(text)} />
          </ListItem>
        ))}
      </List>
      <div className={classes.logout}>
        <LanguageSelect />
        <Fab
          className={classes.langueSelect}
          size="small"
          color="secondary"
          aria-label="Add"
        >
          <Exit onClick={logoutFunc} />
        </Fab>
      </div>
    </div>
  );
  return (
    <div>
      <div className={classes.root}>
        <div
          tabIndex={0}
          role="button"
          // onClick={e => setLeft(e, false)}
          className={classes.menu}
        >
          {sideList}
        </div>
        <div className={classes.restApp}>
          <AppRouter />
        </div>
      </div>
    </div>
  );
}

BaseApp.propTypes = {
  idAvatar: PropTypes.number,
  pseudo: PropTypes.string
};
BaseApp.defaultProps = { idAvatar: 1, pseudo: "pseudo" };

const mapStateToProps = state => ({
  idAvatar: state.Auth.idAvatar,
  pseudo: state.Auth.pseudo
});

export default connect(mapStateToProps)(BaseApp);
