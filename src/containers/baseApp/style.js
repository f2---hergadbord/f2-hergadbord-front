import { makeStyles } from "@material-ui/styles";

const Style = makeStyles({
  root: {
    flexGrow: 1
  },
  grow: {
    flexGrow: 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  },
  menu: {
    width: "230px",
    backgroundColor: "rgb(209,214,241)",
    height: "100%",
    position: "fixed",
    "@media (max-width:887px)": {
      width: "100%",
      position: "unset",
      paddingBottom: "20px"
    }
  },
  toolbar: {
    backgroundColor: "",
    display: "flex",
    position: "relative",
    alignItems: "center"
  },
  iconHome: {
    marginTop: "30px",
    maxWidth: "70px"
  },
  list: {
    textAlign: "center"
  },
  restApp: {
    width: "calc(100% - 230px)",
    right: 0,
    height: "100vh",
    position: "absolute",
    "@media (max-width:887px)": {
      width: "100%",
      position: "unset",
      height: "auto"
    }
  },
  lines: {
    display: "block",
    border: "none",
    color: "white",
    height: "0.5px",

    background:
      "-webkit-gradient(radial, 50% 50%, 0, 50% 50%, 350, from(#000), to(rgba(0, 0, 255, 0)))"
  },
  myli: {
    textAlign: "center !important"
  },
  langueSelect: {
    marginLeft: "10px !important"
  }
});

export default Style;
