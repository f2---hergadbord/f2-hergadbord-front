import React, { useContext } from "react";
import { I18nContext } from "./translate";

import Style from "./style";

const LanguageSelect = () => {
  const classes = Style();
  const { langCode, dispatch } = useContext(I18nContext);
  const onLanguageSelect = e => {
    dispatch({ type: "setLanguage", payload: e.target.value });
  };

  const renderOption = code => (
    <option value={code} defaultValue={code === langCode}>
      {code}
    </option>
  );

  return (
    <select onChange={onLanguageSelect} className={classes.main}>
      {renderOption("en")}
      {renderOption("fr")}
    </select>
  );
};

export default LanguageSelect;
