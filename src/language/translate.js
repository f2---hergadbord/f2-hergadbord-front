import React, { useReducer } from "react";
import PropTypes from "prop-types";

import EN from "./en.json";
import FR from "./fr.json";

const translations = {
  en: EN,
  fr: FR
};

const getTranslate = langCode => key => translations[langCode][key] || key;

const initialState = {
  langCode: "en",
  translate: getTranslate("en")
};

export const I18nContext = React.createContext(initialState);

export const reducer = (state, action) => {
  switch (action.type) {
    case "setLanguage":
      return {
        langCode: action.payload,
        translate: getTranslate(action.payload)
      };
    case "getLanguage":
      return {
        state
      };

    default:
      return { ...initialState };
  }
};

export const I18nContextProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <I18nContext.Provider value={{ ...state, dispatch }}>
      {children}
    </I18nContext.Provider>
  );
};

I18nContextProvider.propTypes = {
  children: PropTypes.element.isRequired
};
