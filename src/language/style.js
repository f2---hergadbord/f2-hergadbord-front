import { makeStyles } from "@material-ui/styles";

const Style = makeStyles({
  main: {
    fontSize: "11px",
    fontFamily: "sans-serif",
    fontWeight: 400,
    color: "#444",
    lineHeight: 1.3,
    padding: ".2em 1em .2em 1em",
    maxWidth: "100%",
    margin: 0,
    border: "none",
    borderRadius: "3px",
    MozAppearance: "none",
    WebkitAppearance: "none",
    appearance: "none"
  }
});

export default Style;
