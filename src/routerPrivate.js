import React from "react";
import { connect } from "react-redux";
import { withRouter, Route, Switch } from "react-router-dom";

import NotFound from "./containers/notFound";
import About from "./containers/about";
import Parameters from "./containers/parameters";
import Dashboard from "./containers/dashboard";

const AppRouter = () => {
  return (
    <Switch>
      <Route path="/dashboard" component={Dashboard} />
      <Route path="/settings" component={Parameters} />
      <Route exact path="/about" component={About} />
      <Route component={NotFound} />
    </Switch>
  );
};

export default withRouter(connect(state => state)(AppRouter));
