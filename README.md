This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Slock Front-end

Slock is the new slack! This is the frontend of Slock.
It's written in react.js using redex-saga !

#### Installation

You will need to have the following installed :

- Node.js

#### Setup

You will need to create .env file at the root with the following env variables :

- REACT_APP_CLIENT_HOST (ex:http://localhost:3000)
- REACT_APP_SERVEUR_HOST (ex:http://localhost:4000)

### Launch

To run it :

- npm i
- npm start

And enjoy !

## How to deploy

Each push on the master branch will trigger Gitlab CI/CD which will execute 2 stages :

- Test => run lint and unit tests, if one of those fail it will not go further
- Deploy => Deploy the app on Heroku !

As simple as that ;)

### `npm i`

Will install all modules needed in folder node_modules.<br>

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## App on heroku

The frontend will be accessed at : https://f2-hergadbord-front.herokuapp.com/

We use free version of Heroku (limited connection). So if you experience an issue
regarding this, let us know!
